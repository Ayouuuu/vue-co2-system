var chance = require('chance').Chance()
var generateData = {
  date: [...Array(31).keys()].map((k) => k + 1),
  data: [...Array(31)].map((k) => {
    return chance.integer({ min: 50, max: 300 })
  })
}

module.exports = [
  // 获取发电数据
  {
    url: '/vue-element-admin/generate/',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: generateData
      }
    }
  },
  // 获取用水量
  {
    url: '/vue-element-admin/water/',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          cold: [...Array(31)].map(() => chance.integer({ min: 20, max: 100 })),
          hot: [...Array(31)].map(() => chance.integer({ min: 20, max: 100 })),
          shop: [...Array(31)].map(() => chance.integer({ min: 50, max: 150 }))
        }
      }
    }
  },
  // 获取碳排放
  {
    url: '/vue-element-admin/co2/',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          gas: [...Array(31)].map(() => chance.integer({ min: 1, max: 10 })),
          coal: [...Array(31)].map(() => chance.integer({ min: 2, max: 5 })),
          other: [...Array(31)].map(() => chance.integer({ min: 4, max: 20 }))
        }
      }
    }
  }
]
