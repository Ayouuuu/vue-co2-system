import { lineChartData } from './index.vue';

export default (await import('vue')).default.extend({
name: 'DashboardAdmin',
components: {
PanelGroup,
PieChart,
BarChart,
ElectricityLine,
GenerateBar
},
data() {
return {
lineChartData: lineChartData.newVisitis,
type: ''
};
},
created() { },
...Array(31).keys()
});
