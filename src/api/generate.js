import request from '@/utils/request'

export function generate() {
  return request({
    url: '/vue-element-admin/generate/',
    method: 'get'
  })
}
export function co2() {
  return request({
    url: '/vue-element-admin/co2/',
    method: 'get'
  })
}
export function water() {
  return request({
    url: '/vue-element-admin/water/',
    method: 'get'
  })
}
