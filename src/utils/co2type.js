const type = [
  { name: '石灰水泥沙浆', unit: 'm3', co2e: 0.7302 },
  { name: '加气混凝土砌块B06', unit: 'm3', co2e: 0.25 },
  { name: '水泥砂浆', unit: 'm3', co2e: 0.7302 },
  { name: '不隔热金属型材', unit: 'm3', co2e: 0.254 },
  { name: '6中透光Low-E+24空气+12透明', unit: 't', co2e: 2.84 },
  { name: '塑料框单层实体门', unit: 'm2', co2e: 0.254 },
  { name: '聚氯乙烯卷材', unit: 't', co2e: 7.3 },
  { name: '细石混凝土', unit: 'm3', co2e: 0.295 },
  { name: '防水卷材', unit: 't', co2e: 0.9514 },
  { name: '钢筋', unit: 't', co2e: 2.34 },
  { name: '混凝土', unit: 't', co2e: 0.295 }
]

const template = [
  { title: '化石燃料碳排放计算', data: type.slice(0, 5).map((k) => createDataByType(k)) }
]

function createData(name, unit, co2e) {
  return {
    name: name,
    used: '',
    unit: unit,
    co2e: co2e,
    sum: 0
  }
}

function createDataByType(type) {
  return createData(type.name, type.unit, type.co2e)
}
export default {
  type,
  template
}
